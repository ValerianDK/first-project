import pandas as pd
from sklearn.linear_model import LinearRegression
from yaml import SafeLoader
import click
import yaml
from datetime import datetime
from joblib import dump
import random

data_path = 'data/processed/train.csv'
model_path = 'models/model_1.joblib'
params = yaml.safe_load(open("params.yaml"))["preprocess"]
random.seed(params["seed"])


@click.command()
@click.option("--config", default="params/train.yaml")
def train(config):
    df = pd.read_csv(data_path)
    X = df['total_meters'].to_numpy().reshape(-1, 1)
    y = df['price']

    model = LinearRegression()
    model.fit(X, y)
    dump(model, model_path)


def generate_train_report(X, df, model, report_path, y):
    k = model.coef_[0]
    b = model.intercept_
    r2 = model.score(X, y)
    print("Coef: ", model.coef_)
    report = [
        f'Time: {datetime.now()}\n',
        f'Training data len: {len(df)}\n',
        f'Formula: Price = {round(k)} * Area + {round(b)}\n',
        f'R2: {r2}\n'
    ]
    with open(report_path, 'w') as f:
        f.writelines(report)


if __name__ == "__main__":
    train()
